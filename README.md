# Termocupla tipo K

<img src="https://microcontrollerslab.com/wp-content/uploads/2021/12/MAX6675-modulwe-with-k-type-thermocouple.jpg" alt="fig. 1" width="350"/>

## Descripción

Este módulo contiene una termocupla tipo K y un circuito integrado [MAX6675](https://www.maximintegrated.com/en/products/interface/signal-integrity/MAX6675.html)
que digitaliza la señal de la termocupla. El conversor envía la información mediante una interfaz SPI.

<img src="img/physics.png" alt="fig. 1" width="450"/>

## Especificaciones
- Resolución A/D: 12 bits
- Resolución en temperatura: 0.25°C
- Voltage de operación: 5V

El módulo tiene 4 terminales (pines) de conexión:
- 1. SO: salida de datos
- 2. CS: habilitación del módulo
- 3. SCK: señal de reloj
- 4. VCC: conectar a +5V
- 4. GND: conectar a GND

## Diagrama de conexión con Arduino

<img src="img/Schematic.png" alt="fig. 1" width="450"/>

## Carga del firmware

Esta aplicación es muy sencilla. Lee los datos del sensor y los envía a la computadora para visualizarlos.

1. Descargar a su PC el [firmware](firmware/firmware.ino)
2. Abrir la aplicación [Arduino IDE](https://www.arduino.cc/en/software)

<img src="img/arduinoIDE_1.png" alt="fig. 1" width="300"/>

3. Abrir el código que descargó en el punto 1.
4. Conectar el Arduino a la computadora con el cable USB.
5. Seleccionar el puerto serie correspondiente al Arduino.

<img src="img/arduinoIDE_2.png" alt="fig. 1" width="300"/>

6. Seleccionar la placa Arduino que corresponde.

<img src="img/arduinoIDE_3.png" alt="fig. 1" width="300"/>

7. Cargar el firmware.
8. Abrir el *monitor serial* o el *serial plotter*

<img src="img/arduinoIDE_4.png" alt="fig. 1" width="300"/>

9. Asegurarse que la velocidad de comunicación está seteada en 9600 bauds

### AHORA PROBÁ CALENTAR LA TERMOCUPLA!!!
