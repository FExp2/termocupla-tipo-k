#include "max6675.h" 

//Definimos los pines de conexion
int SO = 12;
int CS = 10;
int sck = 13;

//Configuramos el modulo MAX6675
MAX6675 module(sck, CS, SO);

void setup() {
  //Iniciamos la comunicacion por puerto serie a 9600 bauds
  Serial.begin(9600);
}

void loop() {
  //Leemos la temperatura.
  float temperature = module.readCelsius();
  //Enviamos la informacion a la PC por puerto serie
  Serial.print("Temperatura: ");
  Serial.print(temperature);
  Serial.println(F("°C "));   
  delay(1000); //Pausa de 1 segundo
}
